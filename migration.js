const customersIds = require('./meta/customers.json');
const {db} = require('./libs/DbAgent');
const {CUSTOMERS} = require("./meta/collections");

async function fill(customerId){
    if(!await db.get(`${CUSTOMERS}-${customerId}`)){
        await db.set(`${CUSTOMERS}-${customerId}`,JSON.stringify({id: customerId,updated:null}));
    }
}

async function populate(){

    await db.connect();
    await db.flushAll();

    let tasks=[];

    customersIds.forEach((customerId)=>{
        console.log(customerId)
        tasks.push(fill(customerId));
    });

    Promise.all(tasks).then(async ()=>{
        console.log(await db.keys('*'))
        console.log('finished');
        process.exit();
    }).catch(e=>console.error(e));

}

populate();