const fs = require('fs')
const fsPromises = require('fs/promises')

class Logger {

    constructor(path) {
        if (!path) {
            throw new Error('Path is required');
        }
        this.path = path;

        return fs.access( this.path, (err) => {
            console.error(err)
            if (err) {
                if(err.errno === -2){
                    return fs.writeFileSync( this.path,"");
                }else{
                    throw new Error(err);
                }
            }

           return true;
        })
    }

    async write(data){
        await fsPromises.appendFile( this.path, data);
    }
}

module.exports = Logger;