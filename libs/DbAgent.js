require('dotenv').config();
const {createClient} = require('redis');
const client = createClient({
    host: process.env.REDIS_HOST ?? '127.0.0.1',
    port:  process.env.REDIS_PORT ?? 3679,
    password:  process.env.REDIS_PASSWORD ??  'root',
});
class DbAgent {
    constructor(client) {

        if (!this.client) {
            this.client = client;

        }

    }

    async connect() {

        if(!this.client.connected){
            try{
                await this.client.connect();
            }catch (e) {
                console.error(e)
            }
        }
    }

    async set(key, value) {
        return await this.client.set(key, value);
    }

    async get(key) {
        return await this.client.get(key);
    }

    async del(key) {
        return await this.client.del(key);
    }

    async keys(str) {
        return await this.client.keys(str);
    }

    async flushAll() {
        return await this.client.flushAll();
    }

    async hset(hash, key, value) {
        return await this.client.json.set(hash, key, value);
    }

    async hget(hash, key) {
        return await this.client.json.get(hash, key);
    }

    async hdel(hash, key) {
        return await this.client.hdel(hash, key);
    }

}

module.exports.db = new DbAgent(client);