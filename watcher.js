const dotenv = require('dotenv');
const Logger = require('./libs/Logger');
dotenv.config();
const {db} = require('./libs/DbAgent');
const {hDateTime} = require('./libs/Helpers');
const testLog = new Logger('./logs/test.log');
const errorLog = new Logger('./logs/error.log');

async function init() {

    try {
        const keys = await db.keys('*');
        const currentTimestamp = Math.floor(Date.now() / 1000);

        for (let key in keys) {

            let record = JSON.parse(await db.get(keys[key]));

            if (currentTimestamp - record.updated > 60 && !record.alerted) {

                await alertLog(record.id)
                await db.set(keys[key], JSON.stringify({...record, alerted: true}));

            } else if (currentTimestamp - record.updated < 60 && record.alerted) {

                await restoreLog(record.id)
                await db.set(keys[key], JSON.stringify({...record, alerted: false}));
            }

        }
    } catch (e) {
        console.error(e)
        await errorLog.write(hDateTime() + '-' + (e).toString() + '\n')
    }
}

async function alertLog(id) {
    await testLog.write(hDateTime() + ` - customer  ${id} was not updated over 1 minute \n`);
}

async function restoreLog(id) {
    await testLog.write(hDateTime() + ` - customer  ${id} in sync again \n`);
}

db.connect().then(() => {
    setInterval(() => {
        init().then(() => {
            console.log('finished ' + Date.now());
        })
    }, 1000);
});
