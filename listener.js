const dotenv = require('dotenv');
const fs = require('fs/promises');
const Logger = require('./libs/Logger');
const {hDateTime} = require('./libs/Helpers');
const errorLog = new Logger('./logs/test.log');
dotenv.config();

const ac = new AbortController();
const {signal} = ac;

const {db} = require('./libs/DbAgent');
const {CUSTOMERS} = require("./meta/collections");
const STORAGE_PATH = process.env.STORAGE_PATH ?? './test';

async function init() {
    await db.connect();
    try {
        const watcher = fs.watch(STORAGE_PATH, {signal});
        for await (const event of watcher) {

            let fileName = event.filename;
            let [customerId, fileType] = fileName.split('.');

            let record = await db.get(`${CUSTOMERS}-${customerId}`);
            if (fileType === 'batch' && record) {

                // todo: rename file to "on progress" with unique id in order to separate feed
                // todo: read file and handle logic - not part of task

                await db.set(`${CUSTOMERS}-${customerId}`, JSON.stringify({
                    ...JSON.parse(record),
                    updated: Math.floor(Date.now() / 1000)
                }));

            } else {
                let path = `${STORAGE_PATH}/${fileName}`;
                await errorLog.write(hDateTime() + ` - Unrecognized file ${path} was deleted.\n`);
               try{
                   await fs.unlink(path)
               }catch (e){

               }
            }

        }
    } catch (err) {
        if (err.name === 'AbortError')
            return;
        throw err;
    }
}

init();